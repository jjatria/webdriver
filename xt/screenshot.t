use Test;
use WebDriver;

my $wd = WebDriver.new: :4444port, :host<geckodriver>,
    :capabilities(:alwaysMatch('moz:firefoxOptions' => :args('-headless',)));

subtest 'Browser' => {
    $wd.get('http://httpd');

    my Blob $png = $wd.screenshot;
    is $png.subbuf(0, 8), Blob.new( 0x89, |"PNG\r\n".encode, 0x1A, 0x0A ),
        'screenshot looks like a PNG';
}

subtest 'Element' => {
    $wd.get('http://httpd');
    my $elem = $wd.find: 'alert', :using(WebDriver::Selector::LinkText);

    my Blob $png = $elem.screenshot;
    is $png.subbuf(0, 8), Blob.new( 0x89, |"PNG\r\n".encode, 0x1A, 0x0A ),
        'screenshot looks like a PNG';
}

done-testing;
