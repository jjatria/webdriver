use Test;
use WebDriver;

my $wd = WebDriver.new: :4444port, :host<geckodriver>,
    :capabilities(:alwaysMatch('moz:firefoxOptions' => :args('-headless',)));

is $wd.print.subbuf(^5).decode, '%PDF-', 'print looks like a PDF';

# All non-defaults from https://www.w3.org/TR/webdriver/#print-page
is $wd.print(
    background  => True,
    orientation => 'landscape',
    shrinkToFit => False,
    scale       => 2,
).subbuf(^5).decode, '%PDF-', 'print with args looks like a PDF';

throws-like { $wd.print :orientation<foo> }, Exception,
    :message(/'unknown variant `foo`, expected `landscape` or `portrait`'/),
    'print with an invalid arg throws';

done-testing;
