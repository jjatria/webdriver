unit class WebDriver;

my role Request {
    need HTTP::Tiny;

    has $!base is built;

    method !request($method, $path = '', *%content) {
        state $ua = HTTP::Tiny.new:
            :default-headers(:content-type<application/json>),
            :agent("WebDriver/$?DISTRIBUTION.meta()<ver> Raku");

        my %res = $ua.request: $method, $.defined ?? "$!base/$path" !! $path,
            :content(Rakudo::Internals::JSON.to-json: %content);

        my $value = try {
            CATCH { die %res<content>.decode }

            Rakudo::Internals::JSON.from-json( %res<content>.decode ).<value>;
        }

        die "$_: $value<message>" with $value<error>;

        $value;
    }

    method !base64(|c) {
        state &base64-decode = do {
            try require ::('MIME::Base64');
            ::('MIME::Base64') !~~ Failure ?? { ::('MIME::Base64').decode: $^encoded }
            !! {
                my constant %dec = ( 'A'…'Z', 'a'…'z', 0…9, '+', '/' ).antipairs;
                Blob.new: $^encoded.subst(/ '='+ $/).comb\
                    .map({ %dec{$_}.fmt: '%06b' }).join.comb(8)».parse-base(2)
            }
        }

        return base64-decode self!request: |c;
    }
}
also does Request;

my class Element does Request {
    method enabled  of Str      { self!request: 'GET',  'enabled'       }
    method clear    of ::?CLASS { self!request: 'POST', 'clear'; self   }
    method click    of ::?CLASS { self!request: 'POST', 'click'; self   }
    method label    of Str      { self!request: 'GET',  'computedlabel' }
    method role     of Str      { self!request: 'GET',  'computedrole'  }
    method selected of Bool     { self!request: 'GET',  'selected'      }
    method tag      of Str      { self!request: 'GET',  'name'          }
    method text     of Str      { self!request: 'GET',  'text'          }
    method visible  of Bool     { self!request: 'GET',  'displayed'     }

    method attr(Str:D $name) of Str { self!request: 'GET', "attribute/$name" }
    method  css(Str:D $name) of Str { self!request: 'GET',       "css/$name" }
    method prop(Str:D $name) of Str { self!request: 'GET',  "property/$name" }

    method send-keys(Str:D $text) of ::?CLASS {
        self!request: 'POST', 'value', :$text;
        self;
    }

    method screenshot of Blob { self!base64: 'GET', 'screenshot' }
}

enum Selector is export (
    CSS             => 'css selector',
    LinkText        => 'link text',
    PartialLinkText => 'partial link text',
    TagName         => 'tag name',
    XPath           => 'xpath',
);

# TODO SetHash[Str] once https://github.com/rakudo/rakudo/issues/2544
my SetHash $sessions;

submethod BUILD(:$host = '127.0.0.1', :$port, :%capabilities) {
    $!base = "http://$host:$port/session";

    my %res = self!request: 'POST', '', :%capabilities;

    $sessions{ $!base ~= "/%res<sessionId>" }++;
}

method accept-alert  of ::?CLASS { self!request: 'POST', 'alert/accept';  self }
method back          of ::?CLASS { self!request: 'POST', 'back';          self }
method dismiss-alert of ::?CLASS { self!request: 'POST', 'alert/dismiss'; self }
method forward       of ::?CLASS { self!request: 'POST', 'forward';       self }
method refresh       of ::?CLASS { self!request: 'POST', 'refresh';       self }
method source        of Str      { self!request: 'GET',  'source' }
method title         of Str      { self!request: 'GET',  'title'  }
method url           of Str      { self!request: 'GET',  'url'    }

method active-element of Element {
    self!element: self!request: 'GET', 'element/active'
}

multi method alert-text of Str:D { self!request: 'GET', 'alert/text' }
multi method alert-text(Str:D $text) of ::?CLASS {
    self!request: 'POST', 'alert/text', :$text;
    self;
}

multi method cookie              { self!request: 'GET', 'cookie' }
multi method cookie(Str:D $name) { self!request: 'GET', "cookie/$name" }
multi method cookie(Str:D $name, Str:D $value, *%args) {
    self!request: 'POST', 'cookie', cookie => { :$name, :$value, |%args };
}

multi method delete-cookie              { self!request: 'DELETE', 'cookie' }
multi method delete-cookie(Str:D $name) { self!request: 'DELETE', "cookie/$name" }

method find(Str:D $value, Selector:D :$using = Selector::CSS) of Element {
    self!element: self!request: 'POST', 'element', :$using, :$value;
}

method find-all(Str:D $value, Selector:D :$using = Selector::CSS) {
    self!request("POST", 'elements', :$using, :$value).map: { self!element: $_ };
}

method get(Str:D $url) of ::?CLASS { self!request: 'POST', 'url', :$url; self }

method js(Str:D $script, *@args, Bool :$async) {
    self!request: 'POST', "execute/{ 'a' if $async }sync", :$script, :@args;
}

method status of Hash {
    # /status is the only path without the session prefix, so suppress it.
    temp $!base = $!base.subst: / '/session/' .* /;

    self!request: 'GET', 'status';
}

multi method timeouts of Hash { self!request: 'GET', 'timeouts' }
multi method timeouts(*%timeouts where ?%timeouts) of ::?CLASS {
    self!request: 'POST', 'timeouts', |%timeouts;
    self;
}

method print(*%args) of Blob { self!base64: 'POST', 'print', |%args }
method screenshot    of Blob { self!base64: 'GET',  'screenshot'    }

method !element($pair) {
    constant ID = 'element-6066-11e4-a52e-4f735466cecf';

    Element.new: :base("$!base/element/$pair{ID}");
}

method delete-session { try { self!request: 'DELETE'; $sessions{$!base}-- } }
method        DESTROY { $.delete-session }
                  END { $?CLASS!request: 'DELETE', $_ for $sessions.keys }
