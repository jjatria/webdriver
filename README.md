# WebDriver

[![pipeline]](https://gitlab.com/raku-land/webdriver/commits/master)

WebDriver is a remote control interface for web browsers.

## SYNOPSIS

```raku
use WebDriver;

my $wd = WebDriver.new :4444port;

# Go to Google.
$wd.get: 'https://www.google.com';

# Run a search for "raku".
$wd.find('input[name=q]').send-keys: 'raku';
$wd.find('input[value="Google Search"]').click;
 
# Click the first result.
$wd.find('h3').click;
 
# Save a screenshot of the page.
spurt 'screenshot.png', $wd.screenshot;
```

## METHODS

### Actions

#### find

```raku
method find(Str:D $value, Selector:D :$using = Selector::CSS) of Element
```

#### find-all

```raku
method find-all(Str:D $value, Selector:D :$using = Selector::CSS)
```

### Alerts

#### accept-alert

```raku
method accept-alert of ::?CLASS
```

Accept an alert.

#### alert-text

```raku
multi method alert-text              of Str
multi method alert-text(Str:D $text) of ::?CLASS
```

Get or set the text of an alert.

#### dismiss-alert

```raku
method dismiss-alert of ::?CLASS
```

Dismiss an alert.

### Navigation

#### back

```raku
method back of ::?CLASS
```

#### forward

```raku
method forward of ::?CLASS
```

#### get

```raku
method get(Str:D $url) of ::?CLASS
```

#### refresh

```raku
method refresh of ::?CLASS
```

## SEE ALSO

[WebDriver Spec](https://www.w3.org/TR/webdriver/)

[pipeline]: https://gitlab.com/raku-land/webdriver/badges/master/pipeline.svg
